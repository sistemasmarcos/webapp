import * as React from 'react';
import {observer, inject} from 'mobx-react';
import DashboardStore from './store';
import UserStore from '../../global-stores/UserStore';
import {setTitle} from '../../libs/titleManager';

export interface DashboardProps {
  userStore: UserStore;
}

@inject('userStore')
@observer
class Dashboard extends React.Component<DashboardProps> {
  public store: DashboardStore;
  constructor(props: DashboardProps) {
    super(props);
    setTitle('Dashboard');
    this.store = new DashboardStore();
  }
  render() {
    return (<div>
      Dashboard - Role: {this.props.userStore.role}
    </div>);
  }
}

export default Dashboard;