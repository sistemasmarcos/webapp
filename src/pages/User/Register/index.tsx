import * as React from 'react';
import {inject, observer} from 'mobx-react';
import UserStore from '../../../global-stores/UserStore';
import RegisterStore from './store';
import {register} from '../../../api/user';
import { Redirect } from 'react-router';

export interface RegisterProps {
  userStore: UserStore;
}

@inject('userStore')
@observer
class Register extends React.Component<RegisterProps> {
  public store: RegisterStore;
  constructor(props: RegisterProps){
    super(props);
    this.store = new RegisterStore();
  }
  render() {
    const store = this.store;
    if (this.props.userStore.loggedIn === true) {
      return (<Redirect to='/dashboard' />);
    }
    return (
      <div>
        <section className="hero is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-6 is-offset-3">
              <h3 className="title">Cadastro</h3>
              <div className="box">
                <div>
                  {store.errorMessage !== '' &&
                    <div className='notification is-warning'>{store.errorMessage}</div>
                  }
                  <div className="field">
                    <div className="control">
                      <input className="input" type="text" placeholder="Usuário"
                        value={store.usuario} onChange={e => store.usuario = e.target.value}
                        autoFocus />
                    </div>
                  </div>

                  <div className="field">
                    <div className="control">
                      <input className="input" type="password" placeholder="Senha"
                      value={store.senha} onChange={e => store.senha = e.target.value} />
                    </div>
                  </div>

                  <div className="field">
                    <div className="control">
                      <input className="input" type="text" placeholder="Nome"
                      value={store.nome} onChange={e => store.nome = e.target.value} />
                    </div>
                  </div>

                  <div className="field">
                    <div className="control">
                      <input className="input" type="email" placeholder="Email"
                        value={store.usuario} onChange={e => store.usuario = e.target.value}
                        autoFocus />
                    </div>
                  </div>

                  <div className="field">
                    <div className="control">
                      <input className="input" type="tel" placeholder="Telefone"
                      value={store.telefone} onChange={e => store.telefone = e.target.value} />
                    </div>
                  </div>
                  
                  <button className={'button is-block is-info is-fullwidth' + 
                        (this.store.loading === true ? ' is-loading' : '')}
                    onClick={() => register(store, this.props.userStore)}>Cadastrar</button>
                </div>
              </div>
              <p className="has-text-grey">
                <a href="../">Cadastre-se</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="../">Esqueceu sua senha?</a>
              </p>
            </div>
          </div>
        </div>
      </section>
      </div>
    );
  }
}

export default Register;