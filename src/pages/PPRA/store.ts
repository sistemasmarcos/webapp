import {observable, action, computed} from 'mobx';

class PPRAStore {
  @observable public nome: string;
  @observable public idade: string;
  constructor() {
    this.nome = '';
    this.idade = '';
  }
}

export default PPRAStore;