import Form, { FormParameters } from '../form-management/Form';
import { createDescricaoField } from '../forms/Forms';

export const AgentesQuimicos = new Form(
  {
      dbTable: 'agentesQuimicos',
      singleTitle: 'Agente Químico',
      pluralTitle: 'Agentes Químicos',
      fields: [
          createDescricaoField(),
      ],
      tableVisibleFields: [
          createDescricaoField(),
      ],
  } as FormParameters);

export default AgentesQuimicos;
