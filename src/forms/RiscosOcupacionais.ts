import Form, { FormParameters } from '../form-management/Form';
import { createDescricaoField } from '../forms/Forms';

const RiscosOcupacionais = new Form({
  dbTable: 'riscosOcupacionais',
  singleTitle: 'Risco Ocupacional',
  pluralTitle: 'Riscos Ocupacionais',
  fields: [
    createDescricaoField(),
  ],
  tableVisibleFields: [
    createDescricaoField(),
  ],
} as FormParameters);

export default RiscosOcupacionais;
