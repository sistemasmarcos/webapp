import * as React from 'react';
import {observer} from 'mobx-react';
import Form from '../../form-management/Form';
import FormBuilderField from './Field';
import FormBuilderStore from './store';
import './style.scss';

export interface FormBuilderProps {
  form: Form;
}

@observer
class FormBuilder extends React.Component<FormBuilderProps, any> {
  public store: FormBuilderStore;
  constructor(props: FormBuilderProps) {
    super(props);
    this.store = new FormBuilderStore();
  }
  componentWillMount() {
    this.props.form.refreshData();
  }
  render() {
    const store = this.store;
    const form = this.props.form;
    const fields = form.fields.map((field) => <FormBuilderField key={field.title} field={field}/>);

    return (<div>
      <div className="card">
            <div className="card-header">
                <h3 className="card-header-title">{form.singleTitle}</h3>
            </div>
            <div className="card-content columns is-multiline">
                {fields}
                    <button className={'button is-success ' + 
                            (form.saving === true ? 'is-loading' : '')}
                      type="button" onClick={() => form.saveItem()}>{form.editingId === '' ? 'Adicionar' : 'Salvar'}</button>
                    {form.editingId === '' ? (
                      <button className="button is-warning" type="button" onClick={() => form.resetFields()}>Limpar Campos</button>
                    ) : (
                      <button className="button is-danger" type="button" onClick={() => form.resetFields()}>Cancelar</button>
                    )}
            </div>
        </div>
        <div className="card">
            <div className="card-header">
                <h3 className="card-header-title">Lista de {form.pluralTitle}</h3>
            </div>
            <div className="card-content">
                <div className="div-tabela">
                    <table className="table is-fullwidth is-bordered is-striped">
                        <thead>
                            <tr>
                              {form.tableVisibleFields.map(field => (
                                <th key={field.title}>{field.title}</th>
                              ))}
                              <th></th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>
                          {form.data.map(item => (
                            <tr key={item._id}>
                              {form.tableVisibleFields.map(field => (
                                <td key={field.title}>
                                  {item[field.dbColumn]}
                                </td>
                              ))}
                              <td className="is-narrow">
                                <button className="button is-warning" type="button"
                                  onClick={() => form.editItem(item)}>Editar</button>
                              </td>
                              <td className="is-narrow">
                                <button className={'button is-danger' + 
                                        (form.deletingIds.some((x) => x === item._id) ? ' is-loading' : '')} 
                                  type="button" onClick={() => form.deleteItem(item)}>Excluir</button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>);
  }
}

export default FormBuilder;