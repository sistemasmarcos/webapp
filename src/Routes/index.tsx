import * as React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Authorization from './Authorization';
import * as Loadable from 'react-loadable';

const Loading = () => (<div>Carregando...</div>);

const HomePage = Loadable({
  loader: () => import('../pages/HomePage'),
  loading: Loading
});

const LoginPage = Loadable({
  loader: () => import('../pages/User/Login'),
  loading: Loading
});
const RegisterPage = Loadable({
  loader: () => import('../pages/User/Register'),
  loading: Loading
});

const Dashboard = Loadable({
  loader: () => import('../pages/Dashboard'),
  loading: Loading
});

const PPRA = Loadable({
  loader: () => import('../pages/PPRA'),
  loading: Loading
});

const User = Authorization(['user']);
const Admin = Authorization(['admin']);
const AnyLogged = Authorization(['user', 'admin']);

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path='/login' component={LoginPage} />
      <Route path='/register' component={RegisterPage} />
      <Route path='/dashboard' component={Dashboard} />
      <Route path="/ppra" component={User(PPRA)} />
    </Switch>
  </Router>
);

export default Routes;