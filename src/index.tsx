import * as React from 'react';
import * as ReactDOM from 'react-dom';
import axios from 'axios';
import {Provider} from 'mobx-react';
import App from './App';
import './style.scss';
import {title} from './app-config';
import UserStore from './global-stores/UserStore';

const userStore = new UserStore();

if (sessionStorage.getItem('authkey') !== null) {
  userStore.loggedIn = true;
}

axios.interceptors.request.use((config) => {
  config.headers = {'authkey': sessionStorage.getItem('authkey')};
  return config;
}, (error) => {
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  if (response.headers.authkey) {
    sessionStorage.setItem('authkey', response.headers['authkey']);
  }
  return response;
}, (error) => {
  if (error.response.status === 401) {
    userStore.logout();
  }
  return Promise.reject(error);
});

document.title = title;

ReactDOM.render(
  <Provider userStore={userStore}>
    <App/>
  </Provider>,
  document.getElementById('app')
);