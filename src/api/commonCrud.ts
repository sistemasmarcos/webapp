import axios from 'axios';
import {apiUrl} from '../app-config';

const prefix = apiUrl + 'gerenciamento/';

export const getAll = (table: string) => axios.get(`${prefix}${table}`);

export const insertItem = (table: string, data: any) => axios.post(`${prefix}${table}`, data);

export const updateItem = (table: string, data: any) => axios.patch(`${prefix}${table}`, data);

export const deleteItem = (table: string, data: any) => axios.delete(`${prefix}${table}/${data._id}`);
