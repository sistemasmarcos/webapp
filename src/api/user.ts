import axios from 'axios';
import {apiUrl} from '../app-config';
import LoginStore from '../pages/User/Login/store';
import UserStore from '../global-stores/UserStore';
import RegisterStore from '../pages/User/Register/store';

export const login = (data: LoginStore, userStore: UserStore) => {
  if (data.usuario === '' || data.senha === '') {
    data.errorMessage = 'Todos os campos devem ser preenchidos!';
    return;
  }
  data.loading = true;
  axios.post(apiUrl + 'auth/login', {
    usuario: data.usuario,
    senha: data.senha,
    lembrarme: data.lembrarme
  })
  .then(r => {
    if (r.status === 200) {
      userStore.loggedIn = true;
    } else {
      data.errorMessage = 'Login e/ou senha inválidos!';
    }
  })
  .catch(() => {
    data.errorMessage = 'Login e/ou senha inválidos!';
  })
  .finally(() => {
    data.loading = false;
  })
};

export const register = (data: RegisterStore, userStore: UserStore) => {
  data.loading = true;
  axios.post(apiUrl + 'auth/register', {
    usuario: data.usuario,
    senha: data.senha,
    nome: data.nome,
    telefone: data.telefone
  })
  .then(r => {
    if (r.status === 200) {
      userStore.loggedIn = true;
    } else {
      data.errorMessage = 'Erro ao efetuar cadastro. Tente novamente.';
    }
  })
  .catch(() => {
    data.errorMessage = 'Erro ao efetuar cadastro. Tente novamente.';
  })
  .finally(() => {
    data.loading = false;
  })
};