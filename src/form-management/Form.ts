import {
    observable as obs,
    action as act
} from 'mobx';
import FormField from './FormField';
import * as FormManagement from './FormManagement';
import * as crud from '../api/commonCrud';

interface FormParameters {
    dbTable: string;
    singleTitle: string;
    pluralTitle: string;
    fields: FormField[];
    tableVisibleFields: FormField[];
    data: any[];
}

class Form {
    @obs public dbTable: string;
    @obs public singleTitle: string;
    @obs public pluralTitle: string;
    @obs public fields: FormField[];
    @obs public tableVisibleFields: FormField[];
    @obs public data: any[]; // dados atuais
    @obs public editingId: string;
    @obs public deletingIds: string[];
    @obs public errored: boolean;
    @obs public loading: boolean;
    @obs public saving: boolean;
    @obs public DataRefreshedCallback!: () => void;
    constructor(params: FormParameters) {
        this.dbTable = params.dbTable;
        this.singleTitle = params.singleTitle;
        this.pluralTitle = params.pluralTitle;
        this.fields = params.fields;
        this.tableVisibleFields = params.tableVisibleFields;
        this.data = [];
        this.editingId = '';
        this.deletingIds = [];
        this.errored = false;
        this.loading = false;
    }
    @act public resetFields() {
        for (const field of this.fields) {
            if (field instanceof FormManagement.SelectField && field.availableValues.length > 0) {
                field.value = field.availableValues[0];
            } else {
                field.value = field.defaultValue;
            }
        }
        this.editingId = '';
    }
    @act public refreshData() {
        this.loading = true;
        return crud.getAll(this.dbTable)
            .then((r) => {
                this.data = r.data;
                if (this.DataRefreshedCallback) {
                    this.DataRefreshedCallback();
                }
            })
            .catch((e) => {
                window.console.log(e);
            })
            .finally(() => {
                this.errored = false;
                this.loading = false;
                this.clearDeletingIds();
            });
    }
    @act public clearDeletingIds() {
        this.deletingIds = this.deletingIds.filter((x) => this.data.some((y) => y._id === x));
    }
    @act public resetValues() {
        for (const field of this.fields) {
            field.value = field.defaultValue;
        }
    }
    @act public saveItem() {
        this.saving = true;
        let newObj = {};
        for (const field of this.fields) {
            newObj = { ...newObj, [field.dbColumn]: field.value };
        }
        let promise;
        if (this.editingId === '') {
            promise = crud.insertItem(this.dbTable, newObj);
        } else {
            newObj = { ...newObj, _id: this.editingId };
            promise = crud.updateItem(this.dbTable, newObj);
        }
        promise.then((r) => {
            const editing = this.editingId !== '';
            if ((!editing && r.status === 201) || (editing && r.status === 200)) {
                this.refreshData();
                this.resetFields();
                //this.$snotify.success('Salvo com sucesso!', { showProgressBar: false });
            } else {
                window.alert(`Não foi possível salvar o ${this.singleTitle}!`);
            }
        })
            .catch((r) => {
                window.alert(`Não foi possível salvar o ${this.singleTitle}!`);
            })
            .finally(() => {
                this.saving = false;
            });
    }
    @act public editItem(item: any) {
        for (const field of this.fields) {
            field.value = item[field.dbColumn];
        }
        this.editingId = item._id;
    }
    @act public deleteItem(item: any) {
        // if (this.Form === undefined) {
        //     return;
        // }
        if (window.confirm(`Deseja realmente excluir este ${this.singleTitle}?`)) {
            this.deletingIds.push(item._id);
            crud.deleteItem(this.dbTable, item)
                .then((r) => {
                    if (r.status === 200) {
                        // window.alert('Excluído com sucesso!');
                        this.refreshData();
                        if (this.editingId === item._id) {
                            this.editingId = '';
                        }
                    }
                })
                .catch((r) => {
                    window.alert('Algum erro ocorreu!');
                });
        }
        // this.$snotify.confirm(`Deseja realmente excluir este ${this.Form.singleTitle}?`,
        // {
        //     position: SnotifyPosition.centerCenter,
        //     buttons: [
        //         {text: 'Sim', action: (toastId) => {
        //             if (this.Form === undefined) {
        //                 return;
        //             }
        //             this.Form.deletingIds.push(item._id);
        //             crud.deleteItem(this.Form.dbTable, item)
        //             .then((r) => {
        //                 if (r.status === 200) {
        //                     // window.alert('Excluído com sucesso!');
        //                     if (this.Form !== undefined) {
        //                         this.Form.refreshData();
        //                         if (this.Form.editingId === item._id) {
        //                             this.Form.editingId = '';
        //                         }
        //                     }
        //                 }
        //             })
        //             .catch((r) => {
        //                 window.alert('Algum erro ocorreu!');
        //             });
        //             this.$snotify.remove(toastId.id);
        //         }, bold: false},
        //         {text: 'Não'},
        //     ],
        //     closeOnClick: true,
        // });
    }
}

export { FormParameters };
export default Form;
