interface IRadioValue {
    value: any;
    displayValue: string;
}

export default IRadioValue;
