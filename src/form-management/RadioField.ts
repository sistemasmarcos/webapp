import Form from './Form';
import FormField, { FormFieldParameters } from './FormField';
import IRadioValue from './IRadioValue';
import IColumns from '../form-management/IColumns';

interface RadioFieldParameters {
    dbColumn: string;
    title: string;
    availableValues: IRadioValue[];
    value?: IRadioValue;
    relatedForm?: Form;
    columns: IColumns;
}

class RadioField extends FormField {
    public availableValues: IRadioValue[];
    public value: IRadioValue | undefined;
    constructor(params: RadioFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            relatedForm: params.relatedForm,
            columns: params.columns,
        } as FormFieldParameters);
        this.availableValues = params.availableValues;
        this.value = params.value;
    }
}

export {RadioFieldParameters};
export default RadioField;
